@smoke
Feature: Check Sorting in Web Table
Background:
    Given Launch Web Page
Scenario Outline: Check Ascending and Descending Sort of phoneNumbers in Web Table
	When Click Sort Order <order>
	And Verify Grid Reaction
	Then Web Table must be sorted in Order <order>
Examples:
    |  order       |
    |  descending  |