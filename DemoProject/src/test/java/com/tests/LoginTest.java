package com.tests;

import org.testng.Assert;
import org.testng.annotations.Test;

import pom.pages.HomePage;
import pom.pages.LoginPage;

public class LoginTest extends BaseTest {

	public LoginTest() {
		// TODO Auto-generated constructor stub
	}
	@Test(priority=1)
	public void verifyLoginPageTitleTest() {
		String title = page.getInstance(LoginPage.class).getLoginPageTitle();
		Assert.assertEquals(title,"Transact Sign in");
	}
	@Test(priority=2)
	public void verifyLoginPageHeaderTest() {
		String header = page.getInstance(LoginPage.class).getLoginPageHeader();
		Assert.assertEquals(header,"Transact Sign in");
	}
	@Test(priority=3)
	public void doLoginTest() {
		HomePage homepage = page.getInstance(LoginPage.class).doLogin("INPUTT", "123456");
		String headerHome=homepage.getHomePageHeader();
		Assert.assertEquals(headerHome,"Transact Sign in");
		
	}

}
