package com.tests;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;

import io.github.bonigarcia.wdm.WebDriverManager;
import pom.pages.BasePage;
import pom.pages.Page;

public class BaseTest {

	WebDriver driver;
	public Page page;
	
	@BeforeMethod
	@Parameters(value= {"browser"})
	public void setUpTest(String browser) {
		if(browser.equals("chrome")) {
			WebDriverManager.chromedriver().setup();
			driver=new ChromeDriver();
		}
		else if (browser.equals("firefox")) {
			WebDriverManager.firefoxdriver().setup();
			driver=new FirefoxDriver();
		}
		else {
			System.out.println("No valid browser is passed");
		}
		driver.get("http://localhost:9089/BrowserWeb/");
		try {
		Thread.sleep(6000);
		} catch(InterruptedException e) {
			e.printStackTrace();
			
		}
		page=new BasePage(driver);
	}
	@AfterMethod
	public void tearDown() {
		driver.quit();
	}

}
