package com.tests;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pom.alertHandling.SortingWebTable;

public class sortWebTable {

	public sortWebTable() {
		
	}
	SortingWebTable s=new SortingWebTable();
	WebDriver driver;
	int pageCount;
	String order;
	List<String> phoneNumbers=new ArrayList<String>();
	List<String> phoneNumbersInAscending=new ArrayList<String>();
	List<String> phoneNumbersInDescending=new ArrayList<String>();
	@Given("^Launch Web Page$")
	public void launch_Web_Page() throws Throwable {
		driver=s.launchWebPage();
		pageCount=s.getNumberOfPages(driver);
	    //throw new PendingException();
	}

	@When("^Click Sort Order ascending$")
	public void click_Sort_Order_ascending() throws Throwable {
		
	    //throw new PendingException();
	}

	@When("^Verify Grid Reaction$")
	public void verify_Grid_Reaction() throws Throwable {
		//Get the Phone numbers list from all the pages before sort 
	    
		phoneNumbers=s.fetchPhoneNumbersFromEachPage(driver,pageCount);
		
		
		//Sort the WebTable in Ascending
		driver.findElement(By.xpath("//*[contains(@id,'uiGrid-0009-menu-button')]")).click();
		driver.findElement(By.xpath("//li[@id='menuitem-0']//child::button")).click();
		
	    //throw new PendingException();
	}

	@Then("^Web Table must be sorted in Order ascending$")
	public void web_Table_must_be_sorted_in_Order_ascending() throws Throwable {
		
		//Get the Phone numbers list from all the pages after sort 
		
		phoneNumbersInAscending=s.fetchPhoneNumbersFromEachPage(driver,pageCount);
		System.out.println("phoneNumbersInAscending\n"+phoneNumbersInAscending);
		
		//Check if the list is sorted in ascending
		@SuppressWarnings("unchecked")
		List tmp = new ArrayList(phoneNumbers);
		Collections.sort(tmp);
		System.out.println("tmp\n"+tmp);
		boolean sortedAscending = tmp.equals(phoneNumbersInAscending);
		if(sortedAscending)
			System.out.println("Ascending order");
		else
			System.out.println("Not in Ascending order");

	    //throw new PendingException();
	}

	@When("^Click Sort Order descending$")
	public void click_Sort_Order_descending() throws Throwable {
		//Sort the WebTable in Descending
		driver.findElement(By.xpath("//*[contains(@id,'uiGrid-0009-menu-button')]")).click();
		driver.findElement(By.xpath("//li[contains(@id,'menuitem-1')]//child::button//child::i")).click();
		System.out.println("driver\n"+driver);
		System.out.println("pageCount\n"+pageCount);
		//Get the Phone numbers list from all the pages after sort 
		phoneNumbersInDescending=s.fetchPhoneNumbersFromEachPage(driver,pageCount);
		System.out.println("phoneNumbersInDescending\n"+phoneNumbersInDescending);

		
	}

	@Then("^Web Table must be sorted in Order descending$")
	public void web_Table_must_be_sorted_in_Order_descending() throws Throwable {
		//Check if the list is sorted in descending
		List tmp = new ArrayList(phoneNumbers);
		Collections.sort(tmp, Collections.reverseOrder());
		System.out.println("tmp\n"+tmp);
		boolean sortedDescending = tmp.equals(phoneNumbersInDescending);
		if(sortedDescending)
			System.out.println("Descending order");
		else
			System.out.println("Not in Descending order");
	    //throw new PendingException();
	}


}
