package pom.alertHandling;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SortingWebTable {
	
	public SortingWebTable() {
		
	}
	@SuppressWarnings({ "rawtypes", "deprecation", "unchecked" })
	public void sortWebTable() {
		/*
		WebDriver driver=launchWebPage();		
		int pageCount=getNumberOfPages(driver);
		
		//Get the Phone numbers list from all the pages before sort 
	    List<String> phoneNumbers=new ArrayList<String>();
		phoneNumbers=fetchPhoneNumbersFromEachPage(driver,pageCount);
		
		
		//Sort the WebTable in Ascending
		driver.findElement(By.xpath("//*[contains(@id,'uiGrid-0009-menu-button')]")).click();
		driver.findElement(By.xpath("//li[@id='menuitem-0']//child::button")).click();
		

		//Get the Phone numbers list from all the pages after sort 
		List<String> phoneNumbersInAscending=new ArrayList<String>();
		phoneNumbersInAscending=fetchPhoneNumbersFromEachPage(driver,pageCount);
		System.out.println("phoneNumbersInAscending\n"+phoneNumbersInAscending);

		//Check if the list is sorted in ascending
		@SuppressWarnings("unchecked")
		List tmp = new ArrayList(phoneNumbers);
		Collections.sort(tmp);
		boolean sortedAscending = tmp.equals(phoneNumbersInAscending);
		if(sortedAscending)
			System.out.println("Ascending order");
		else
			System.out.println("Not in Ascending order");

		//Sort the WebTable in Descending
		driver.findElement(By.xpath("//*[contains(@id,'uiGrid-0009-menu-button')]")).click();
		driver.findElement(By.xpath("//li[@id='menuitem-1']//child::button")).click();

		//Get the Phone numbers list from all the pages after sort 
		List<String> phoneNumbersInDescending=new ArrayList<String>();
		phoneNumbersInDescending=fetchPhoneNumbersFromEachPage(driver,pageCount);
		System.out.println("phoneNumbersInDescending\n"+phoneNumbersInDescending);

		//Check if the list is sorted in descending
		Collections.reverse(tmp);
		boolean sortedDescending = tmp.equals(phoneNumbersInDescending);
		if(sortedDescending)
			System.out.println("Descending order");
		else
			System.out.println("Not in Descending order");
		driver.quit();
		*/
		
	}
	public WebDriver launchWebPage() {
		System.setProperty("webdriver.chrome.driver", "C:\\Softwares\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver=new ChromeDriver();
		driver.get("http://demo.automationtesting.in/WebTable.html");
		// Maximize window
		driver.manage().window().maximize();		
		// Delete cookies
		driver.manage().deleteAllCookies();
		return driver;
	}
	public Integer getNumberOfPages(WebDriver driver) {
		String pageCounted=driver.findElement(By.xpath("//*[contains(@class,'ui-grid-pager-max-pages-number ng-binding')]")).getText();
		  //Regular expression to digits
	      String regex = "([0-9]+)";
	      int pageCount=fetchIntegerFromString(regex, pageCounted);
	      return pageCount;
	}
	public Integer fetchIntegerFromString(String regex,String pageCounted) {
	      //Creating a pattern object
	      Pattern pattern = Pattern.compile(regex);
	      //Creating a Matcher object
	      Matcher matcher = pattern.matcher(pageCounted);
	      int pageCount=0;
	      while(matcher.find()) {
	         pageCount=Integer.parseInt(matcher.group());
	      }
	      return pageCount;
	}
	public List<String> fetchPhoneNumbersFromEachPage(WebDriver driver,Integer pageCount) {		
		List<String> phoneNumbers=new ArrayList<String>();
		if(pageCount>1) {				
		for(int j=1;j<=pageCount;j++){
			List<WebElement> phoneList=driver.findElements(By.xpath("//div[contains(@id,'uiGrid-0009-cell')]"));
		for(int i=0;i<phoneList.size();i++) {
			phoneNumbers.add(phoneList.get(i).getText());			
		}
		driver.findElement(By.xpath("//*[contains(@class,'ui-grid-pager-next')]")).click();
		WebElement pageNumber=driver.findElement(By.xpath("//*[contains(@class,'ui-grid-pager-control-input')]"));
		WebDriverWait wait = new WebDriverWait(driver,1);
		wait.until(ExpectedConditions.visibilityOfAllElements(pageNumber));
		}
		driver.findElement(By.xpath("//*[contains(@class,'ui-grid-pager-control-input')]")).sendKeys("1");
		phoneNumbers.removeAll(Arrays.asList("", null));
		}
		return phoneNumbers;
	}
	public static void main(String args[]) {
		SortingWebTable s=new SortingWebTable();
		s.sortWebTable();
	}
}
