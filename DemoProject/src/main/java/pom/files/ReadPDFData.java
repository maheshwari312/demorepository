package pom.files;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ReadPDFData {

	public static void main(String[] args) throws IOException{
		ReadPDFFileData r=new ReadPDFFileData();
		String val=r.ReadPDFDataAsText("C:\\Softwares\\Test Data\\Cucumber-Gherkin-BDD.pdf");
		System.out.println("value"+val);

		String text = "Scenario";
		Pattern p = Pattern.compile(text,Pattern.CASE_INSENSITIVE);
		Matcher m = p.matcher(val);
		if (m.find()) {
			System.out.println(text+ " found in the pdf");
		} else {
			System.out.println(text+ " not found in the pdf");
		}
	}     
	}


