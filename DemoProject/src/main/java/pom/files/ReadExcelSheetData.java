	/**
	 * 
	 */
	package pom.files;
	
	import java.io.FileInputStream;
	import java.io.IOException;
	import java.util.HashMap;
	import java.util.Map;
	
	import org.apache.poi.hssf.usermodel.HSSFWorkbook;
	import org.apache.poi.ss.usermodel.Cell;
	import org.apache.poi.ss.usermodel.DataFormatter;
	import org.apache.poi.ss.usermodel.Row;
	import org.apache.poi.ss.usermodel.Sheet;
	import org.apache.poi.ss.usermodel.Workbook;
	import org.apache.poi.xssf.usermodel.XSSFWorkbook;
	
	/**
	 * @author dmaheshwari
	 *
	 */
	public class ReadExcelSheetData {
	
		/**
		 * @throws IOException
		 * 
		 */
		public ReadExcelSheetData() throws IOException {
	
		}
		public static Workbook readExcel(String path) throws IOException {
			String fileType=path.substring(path.lastIndexOf(".") + 1);
			System.out.println("fileType"+fileType);
			FileInputStream fis = new FileInputStream(path);
			if (fileType.equalsIgnoreCase("xls")) {
				Workbook wb = new HSSFWorkbook(fis);
				return wb;
			}else if(fileType.equalsIgnoreCase("xlsx")) {
				Workbook wb = new XSSFWorkbook(fis);
				return wb;
			}
			return null;	
		}
		public static Map<String, Map<String, String>> setMapData(Workbook wb) throws IOException {
	
			// 1. Read file from external source and count number of data
			//handled in seperate method
			
			Sheet s = wb.getSheetAt(0);
			int celCount = s.getLastRowNum();
	
			// 2. Initialise Hashmap
			Map<String, Map<String, String>> excelFileMap = new HashMap<String, Map<String, String>>();
			Map<String, String> dataMap = new HashMap<String, String>();
	
			// 3. Loop and get Key and Values
			for (int i = 0; i <= celCount; i++) {
				Row row = s.getRow(i);
				// 1st cell as Value
				Cell cValue = row.getCell(i);
				// 0th cell as key
				Cell cKey = row.getCell(0);
	
				// creating formatter using the default locale
				DataFormatter formatter = new DataFormatter();
				// Returns the formatted value of a cell as a String regardless of the cell
				// type.
				String value = formatter.formatCellValue(cValue);
				String key = formatter.formatCellValue(cKey);
	
				// 4. Putting key & value in dataMap
				dataMap.put(key, value);
	
				// 5. Putting dataMap to excelFileMap
				excelFileMap.put("DataSheet", dataMap);
			}
			
			return excelFileMap;
		}
	
		public static String getMapData(String key,String path) throws IOException {
			Workbook wb=readExcel(path);
			Map<String, String> m = setMapData(wb).get("DataSheet");
			String value = m.get(key);
			return value;
		}
	}