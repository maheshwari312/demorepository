/**
 * 
 */
package pom.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * @author dmaheshwari
 *
 */
public class LoginPage extends BasePage{

	//Page Locator
	private By emailId=By.id("signOnName");
	private By password=By.id("password");
	private By loginBtn=By.id("sign-in");
	private By header=By.id("title");
	
	public LoginPage(WebDriver driver) {
		super(driver);
	}
	
	//getters
	/**
	 * @return the email
	 */
	public WebElement getEmail() {
		return getElement(emailId);
	}
	
	/**
	 * @return the password
	 */
	public WebElement getPassword() {
		return getElement(password);
	}
	
	/**
	 * @return the loginBtn
	 */
	public WebElement getLoginBtn() {
		return getElement(loginBtn);
	}
	
	/**
	 * @return the header
	 */
	public WebElement getHeader() {
		return getElement(header);
	}
	
	public String getLoginPageTitle() {
		return getPageTitle();
	}
	public String getLoginPageHeader() {
		return getPageHeader(header);
	}
	//Login with username and password
	public HomePage doLogin(String username, String pwd) {
		getEmail().sendKeys(username);
		getPassword().sendKeys(pwd);
		getLoginBtn().click();
		
		return getInstance(HomePage.class);
	}
	//Login without username and password
	public void doLogin() {
		getEmail().sendKeys("");
		getPassword().sendKeys("");
		getLoginBtn().click();
	}
	public void doLogin(String userCred) {
		if(userCred.contains("username")) {
			getEmail().sendKeys(userCred.split(":")[1].trim());
		}
		else if(userCred.contains("password")){
			getEmail().sendKeys(userCred.split(":")[1].trim());
		}
		getLoginBtn().click();
	}
	
}
