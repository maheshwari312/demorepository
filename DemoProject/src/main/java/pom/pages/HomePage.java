/**
 * 
 */
package pom.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * @author dmaheshwari
 *
 */
public class HomePage extends BasePage{

	private By header=By.className("puxlXr");
	
	public HomePage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	public WebElement getHeader() {
		return getElement(header);
	}
	
	public String getHomePageTitle() {
		return getPageTitle();
	}
	public String getHomePageHeader() {
		return getPageHeader(header);
	}

}
